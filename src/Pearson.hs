module Pearson (
  pearson
) where

import           Data.List (genericLength)

mean :: Fractional a => [a] -> a
mean xs = sum xs / genericLength xs

covariance :: Fractional a => [a] -> [a] -> a
covariance xs ys = mean productXY
    where
     productXY = zipWith (*) [x - mx | x <- xs] [y - my | y <- ys]
     mx        = mean xs
     my        = mean ys

stddev :: Floating a => [a] -> a
stddev xs = sqrt (covariance xs xs)

pearson :: (Floating a) => [a] -> [a] -> a
pearson x y = covariance x y / (stddev x * stddev y)
