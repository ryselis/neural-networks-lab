module Lib
    ( someFunc
    ) where

import           Data.List           (elemIndex, intercalate, tails)
import           Data.List.Split     (splitOneOf)
import           Data.Maybe          (catMaybes, fromJust, fromMaybe, isJust,
                                      isNothing, mapMaybe)
import           FileConfig
import           Pearson             (pearson)
import           Text.Read           (readMaybe)
import           Text.Show.Functions

data Test =
  Test {
  age                     :: Maybe Int,
  sex                     :: Maybe Sex,
  onThyroxine             :: Bool,
  queryOnThyroxine        :: Bool,
  onAntiThyroidMedication :: Bool,
  sick                    :: Bool,
  pregnant                :: Bool,
  thyroidSurgery          :: Bool,
  i131Treatment           :: Maybe Bool,
  queryHypothyroid        :: Bool,
  queryHyperThyroid       :: Bool,
  lithium                 :: Bool,
  goitre                  :: Bool,
  tumor                   :: Bool,
  hypopituitary           :: Maybe Bool,
  psych                   :: Maybe Bool,
  tsh                     :: Maybe Float,
  t3                      :: Maybe Float,
  tt4                     :: Maybe Float,
  t4u                     :: Maybe Float,
  fti                     :: Maybe Float,
  tbg                     :: Maybe Float,
  sickStatus              :: SickStatus
} deriving (Show)

data Sex = Male | Female deriving (Show, Eq)

data SickStatus = Negative | IncreasedBindingProtein | DecreasedBindingProtein | HyperThyroid | T3Toxic | Goitre |
  SecondaryToxic | HypoThyroid | PrimaryHypoThyroid | CompensatedHypoThyroid | SecondaryHypoThyroid |
  ReplacementTherapy | Underreplacement | Overreplacement | Discordant | SickEuthyroid | Sick deriving (Show)

someFunc :: IO ()
someFunc = do
  let dirname = "/home/ryselis/Dokumentai/Neuroniniai tinklai/haskell/neural-networks/thyroid-disease/"
  let filePaths = [dirname ++ filename f | f <- fileConfig]
  fileContent <- mapM readFile filePaths
  let splitContents = concatMap lines fileContent
  let allLines = concat [lines l | l <- splitContents]
  let parsedLines = [splitOneOf ",|" item | item <- allLines]
  let tests = concat [parseFile cont conf | (cont, conf) <- zip fileContent fileConfig]
  -- check if test values are correlated
  let functionsWithTitles = [("tsh", tsh), ("t3", t3), ("tt4", tt4), ("t4u", t4u), ("fti", fti), ("tbg", tbg), ("onThyroxine", boolsToFloats onThyroxine), ("queryOnThyroxine", boolsToFloats queryOnThyroxine), ("onAntiThyroidMedication", boolsToFloats onAntiThyroidMedication), ("sick", boolsToFloats sick), ("pregnant", boolsToFloats pregnant), ("thyroidSurgery", boolsToFloats thyroidSurgery), ("queryHypothyroid", boolsToFloats queryHypothyroid), ("lithium", boolsToFloats lithium), ("goitre", boolsToFloats goitre), ("tumor", boolsToFloats tumor), ("i131Treatment", maybeBoolsToFloats i131Treatment), ("hypopituitary", maybeBoolsToFloats hypopituitary), ("psych", maybeBoolsToFloats psych), ("age", maybeIntsToMaybeFloats age)]
  let functionWithTitleCombinations = [(x, y) | (x:xs) <- tails functionsWithTitles, y <- xs]
  let functionCombinations = [(xfunc, yfunc) | ((xtitle, xfunc), (ytitle, yfunc)) <- functionWithTitleCombinations]
  let titleCombinations = [(xtitle, ytitle) | ((xtitle, xfunc), (ytitle, yfunc)) <- functionWithTitleCombinations]
  let correctCorrelations = [pearson (mapMaybe xfunc goodTests) (mapMaybe yfunc goodTests) | (xfunc, yfunc) <- functionCombinations, let goodTests = [t | t <- tests, isJust (xfunc t) && isJust (yfunc t)]]
  let correlationsWithFunctionNames = zip titleCombinations correctCorrelations
  let relevantCorrelations = [(t, corr) | (t, corr) <- correlationsWithFunctionNames, abs corr > 0.75]
  putStr $ intercalate "\n" [x ++ " <-> " ++ y ++ ": " ++ show corr | ((x, y), corr) <- relevantCorrelations]
  putStr "\n"

parseFile :: String -> Configuration -> [Test]
parseFile fileContent conf =
  [lineToTest line conf | line <- items, not (null line)]
  where
    items = [splitOneOf ",|" item | item <- tests]
    tests = lines fileContent

lineToTest :: [String] -> Configuration -> Test
lineToTest splitLine conf = do
  let ageColumn = ageColumnNumber conf
  let sickColumn = if ageColumn == 0 then 5 else 10
  let pregnantColumn = if ageColumn == 0 then 6 else 9
  let thyroidSurgeryColumn = if ageColumn == 0 then 7 else 6
  let queryHypothyroidColumn = if ageColumn == 0 then 9 else 7
  let lithiumColumn = if ageColumn == 0 then 11 else 12
  let tumorColumn = if ageColumn == 0 then 13 else 11
  let tshColumn = if ageColumn == 0 then 16 else 15
  Test {age=readMaybe (splitLine!!ageColumn) :: Maybe Int,
  sickStatus = parseSickStatus (removeTrailingDot (splitLine!!isSickColumnNumber conf)),
  sex = letterToSex (splitLine!!(ageColumn+1)),
  onThyroxine = letterToBool (splitLine!!(ageColumn+2)),
  queryOnThyroxine = letterToBool (splitLine!!(ageColumn+3)),
  onAntiThyroidMedication = letterToBool (splitLine!!(ageColumn+4)),
  sick = letterToBool (splitLine!!sickColumn),
  pregnant = letterToBool $ splitLine !! pregnantColumn,
  thyroidSurgery = letterToBool $ splitLine !! thyroidSurgeryColumn,
  i131Treatment = if ageColumn == 0 then Just (letterToBool $ splitLine !! 8) else Nothing,
  queryHypothyroid = letterToBool $ splitLine !! queryHypothyroidColumn,
  queryHyperThyroid = letterToBool $ splitLine !! (queryHypothyroidColumn + 1),
  lithium = letterToBool $ splitLine !! lithiumColumn,
  goitre = letterToBool $ splitLine !! (lithiumColumn + 1),
  tumor = letterToBool $ splitLine !! tumorColumn,
  hypopituitary = if ageColumn == 0 then Just (letterToBool $ splitLine !! 14) else Nothing,
  psych = if ageColumn == 0 then Just (letterToBool $ splitLine !! 15) else Nothing,
  tsh = letterToMaybeFloat $ splitLine !! tshColumn,
  t3 = letterToMaybeFloat $ splitLine !! (tshColumn + 2),
  tt4 = letterToMaybeFloat $ splitLine !! (tshColumn + 4),
  t4u = letterToMaybeFloat $ splitLine !! (tshColumn + 6),
  fti = letterToMaybeFloat $ splitLine !! (tshColumn + 8),
  tbg = letterToMaybeFloat $ splitLine !! (tshColumn + 10)
}

letterToSex :: String -> Maybe Sex
letterToSex "M" = Just Male
letterToSex "F" = Just Female
letterToSex x   = Nothing

letterToBool :: String -> Bool
letterToBool "t" = True
letterToBool "f" = False

letterToMaybeFloat :: String -> Maybe Float
letterToMaybeFloat number =
  readMaybe number :: Maybe Float

parseSickStatus :: String -> SickStatus
parseSickStatus "negative"                  = Negative
parseSickStatus "increased binding protein" = IncreasedBindingProtein
parseSickStatus "decreased binding protein" = DecreasedBindingProtein
parseSickStatus "hyperthyroid"              = HyperThyroid
parseSickStatus "T3 toxic"                  = T3Toxic
parseSickStatus "goitre"                    = Goitre
parseSickStatus "secondary toxic"           = SecondaryToxic
parseSickStatus "hypothyroid"               = HypoThyroid
parseSickStatus "primary hypothyroid"       = PrimaryHypoThyroid
parseSickStatus "compensated hypothyroid"   = CompensatedHypoThyroid
parseSickStatus "secondary hypothyroid"     = SecondaryHypoThyroid
parseSickStatus "replacement therapy"       = ReplacementTherapy
parseSickStatus "underreplacement"          = Underreplacement
parseSickStatus "overreplacement"           = Overreplacement
parseSickStatus "discordant"                = Discordant
parseSickStatus "sick-euthyroid"            = SickEuthyroid
parseSickStatus "sick"                      = Sick

removeTrailingDot :: String -> String
removeTrailingDot "" = ""
removeTrailingDot x
  | last x == '.' = init x
  | otherwise = x


maybeBoolToFloat :: Maybe Bool -> Maybe Float
maybeBoolToFloat maybeBool =
  case maybeBool of
  Just value -> boolToFloat value
  Nothing    -> Nothing

boolToFloat :: Bool -> Maybe Float
boolToFloat bool =
  if bool then Just 1 else Just 0

boolsToFloats :: (Test -> Bool) -> Test -> Maybe Float
boolsToFloats func test =
  boolToFloat $ func test

maybeBoolsToFloats :: (Test -> Maybe Bool) -> Test -> Maybe Float
maybeBoolsToFloats func test =
  maybeBoolToFloat $ func test

maybeIntsToMaybeFloats :: (Test -> Maybe Int) -> Test -> Maybe Float
maybeIntsToMaybeFloats func test = do
  let res = func test
  if isNothing res then Nothing else Just (fromIntegral $ fromJust res)

maybeSexToMaybeFloat :: (Test -> Maybe Sex) -> Test -> Maybe Float
maybeSexToMaybeFloat func test = do
  let res = func test
  if isNothing res then Nothing else Just (if fromJust res == Male then 0 else 1)
