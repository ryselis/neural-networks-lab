module FileConfig (
  fileConfig, Configuration, filename, ageColumnNumber, isSickColumnNumber
)
where

data Configuration = Configuration {
  filename           :: String,
  ageColumnNumber    :: Int,
  isSickColumnNumber :: Int
}

fileConfig :: [Configuration]
fileConfig = [
  standardConfig "allbp.data",
  standardConfig "allhyper.data",
  standardConfig "allhypo.data",
  standardConfig "allrep.data",
  standardConfig "dis.data",
  configWithTitleAsFirstColumn "hypothyroid.data",
  configWithTitleAsFirstColumn "sick-euthyroid.data",
  standardConfig "sick.data"
  ]

standardConfig :: String -> Configuration
standardConfig filename = Configuration {
  filename=filename, ageColumnNumber=0, isSickColumnNumber=29
}

configWithTitleAsFirstColumn :: String -> Configuration
configWithTitleAsFirstColumn filename = Configuration {
  filename=filename, ageColumnNumber=1, isSickColumnNumber=0
}
